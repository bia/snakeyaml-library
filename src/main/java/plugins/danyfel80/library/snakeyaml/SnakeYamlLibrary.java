/**
 * 
 */
package plugins.danyfel80.library.snakeyaml;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * A YAML Library for Icy
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public class SnakeYamlLibrary extends Plugin implements PluginLibrary
{
}
